function getDigitalTime(date) {
    var hour = date.getHours()
    var min = date.getMinutes()
    var sec = date.getSeconds()
    hour = padZeroes(hour)
    min = padZeroes(min)
    sec = padZeroes(sec)
    return hour + " : " + min + " : " + sec
}

function padZeroes(i) {
    return i < 10 ? "0" + i : i
}

module.exports = getDigitalTime