async function myFetch(url) {
    try {
      const response = await fetch(url)
      var data = await response.json()
      return data
    } catch(error) {
      console.log(error)
      return null
    }
}

module.exports = myFetch